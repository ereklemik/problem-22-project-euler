const fsModule = require("fs");
const csv = fsModule.readFileSync("./text/names.txt", "utf-8");
const arr = csv.split(",");

function nameScoreCalc(arr) {
  arr.sort();
  let totalSum = 0;

  for (let i = 0; i < arr.length; i++) {
    let letterSum = 0;
    for (let j = 0; j < arr[i].length; j++) {
      letterSum += (arr[i].charCodeAt(j) - 64) * (i + 1);
    }
    letterSum *= i + 1;
    totalSum += letterSum;
  }
  return totalSum;
}

console.log(nameScoreCalc(arr));
